package com.suppliermanagement.controllers.Mail;

/**
 * Created by Naveen Luke Fernando on 2017-08-21.
 */

import com.common.ControlledScreen;
import com.common.ScreenController;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTreeTableView;
import com.suppliermanagement.controllers.SupplierScreens;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import java.net.URL;
import javafx.scene.input.MouseEvent;
import java.util.ResourceBundle;


public class suplyView_Message_Home_CTRL implements Initializable,ControlledScreen {

    ScreenController controller;



    @FXML
    private JFXButton cmp_mail;

    @FXML
    private JFXButton inbox;

    @FXML
    private JFXButton logoutBtn;

    @FXML
    private JFXButton refresh_btn;

    @FXML
    private JFXButton set_btn;

    @FXML
    private JFXTreeTableView<?> email_view;

    @Override
    public void setScreenParent(ScreenController screenParent) {
        controller = screenParent;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

//    @FXML
//    void changeScene(MouseEvent event) {
//
//        switch (((JFXButton) event.getSource()).getId()){
//
//            case "MessageBtn":
//                ScreenController.changeScreen(controller, SupplierScreens.MESSAGE_SCREEN, SupplierScreens.MESSAGE_SCREEN);
//                break;
//
//            case "home":
//                ScreenController.changeScreen(controller, SupplierScreens.MESSAGE_SCREEN, SupplierScreens.DASHBOARD);
//                break;
//            case "pur_btn":
//                ScreenController.changeScreen(controller,SupplierScreens.MESSAGE_SCREEN ,SupplierScreens.PURCHASE_SCREEN );
//                break;
//            case "sup_btn":
//                ScreenController.changeScreen(controller,SupplierScreens.MESSAGE_SCREEN , SupplierScreens.SUPPLIER_SCREEN);
//                break;
//            case "return_btn":
//                ScreenController.changeScreen(controller,SupplierScreens.MESSAGE_SCREEN,SupplierScreens.RETURNS);
//                break;
//            case "warehouse_btn":
//                ScreenController.changeScreen(controller, SupplierScreens.MESSAGE_SCREEN, SupplierScreens.WARE_HOUSE);
//                break;
//            case "rep_btn":
//               ScreenController.changeScreen(controller,SupplierScreens.MESSAGE_SCREEN,SupplierScreens.REPORTS);
//                break;
//
//
//        }
//    }

    }













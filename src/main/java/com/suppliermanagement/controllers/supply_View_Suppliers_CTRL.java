package com.suppliermanagement.controllers;

import com.common.ControlledScreen;
import com.common.ScreenController;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Naveen Luke Fernando on 2017-09-14.
 */

public class supply_View_Suppliers_CTRL implements Initializable,ControlledScreen {

    ScreenController controller;

    @Override
    public void setScreenParent(ScreenController screenParent) {
        controller = screenParent;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


//    @FXML
//    void changeScene(MouseEvent event) {
//
//        switch (((JFXButton) event.getSource()).getId()) {
//
//            case "MessageBtn":
//                ScreenController.changeScreen(controller, SupplierScreens.SUPPLIER_SCREEN, SupplierScreens.MESSAGE_SCREEN);
//                break;
//
//            case "home":
//                ScreenController.changeScreen(controller, SupplierScreens.SUPPLIER_SCREEN, SupplierScreens.DASHBOARD);
//                break;
//            case "pur_btn":
//                ScreenController.changeScreen(controller, SupplierScreens.SUPPLIER_SCREEN, SupplierScreens.PURCHASE_SCREEN);
//                break;
//            case "sup_btn":
//                ScreenController.changeScreen(controller, SupplierScreens.SUPPLIER_SCREEN, SupplierScreens.SUPPLIER_SCREEN);
//                break;
//            case "return_btn":
//                ScreenController.changeScreen(controller, SupplierScreens.SUPPLIER_SCREEN, SupplierScreens.RETURNS);
//                break;
//            case "warehouse_btn":
//                ScreenController.changeScreen(controller, SupplierScreens.SUPPLIER_SCREEN, SupplierScreens.WARE_HOUSE);
//                break;
//            case "rep_btn":
//                ScreenController.changeScreen(controller, SupplierScreens.SUPPLIER_SCREEN, SupplierScreens.REPORTS);
//                break;
//
//
//        }
//    }
}
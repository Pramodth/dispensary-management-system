package com.appointmentscheduling.controllers.AppointmentAgenda;

import com.EntityClasses.Doctor;
import com.EntityClasses.Patient;
import jfxtras.scene.control.agenda.Agenda;

/**
 * Created by Kalan on 9/30/2017.
 */
public class Appointment extends Agenda.AppointmentImplLocal {

    private int id;
    private double charge;
    private Doctor Doctor;
    private Patient Patient;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCharge() { return charge; }

    public void setCharge(double charge) { this.charge = charge; }

    public Doctor getDoctor() { return Doctor;}

    public void setDoctor(Doctor Doctor) { this.Doctor = Doctor; }

    public Patient getPatient() { return Patient;}

    public void setPatient(Patient Patient) { this.Patient = Patient; }


}

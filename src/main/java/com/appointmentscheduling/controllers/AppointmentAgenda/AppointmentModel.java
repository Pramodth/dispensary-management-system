package com.appointmentscheduling.controllers.AppointmentAgenda;

import com.EntityClasses.Doctor;
import com.EntityClasses.Patient;
import com.appointmentscheduling.controllers.Add_Appointment_CTRL.Add_Appointment_CTRL;
import db.UserSession;
import jfxtras.scene.control.agenda.Agenda;
import org.hibernate.Query;
import org.hibernate.Session;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;




public class AppointmentModel {

    private static Session session = UserSession.getSession();

    public static Doctor doctor;
    public static Patient patient;



    public int addNewAppointment(Agenda.AppointmentImplLocal newAppointment){

        com.EntityClasses.Appointment appointment = new com.EntityClasses.Appointment();
        appointment.setStartTime(Timestamp.valueOf(newAppointment.getStartLocalDateTime()));
        appointment.setEndTime(Timestamp.valueOf(newAppointment.getEndLocalDateTime()));
        appointment.setReason(newAppointment.getDescription());
        appointment.setDoctor(doctor);
        appointment.setPatient(patient);
     //   appointment.setCharge(Add_Appointment_CTRL.getcharge());

//        doctor.getAppointments().add(appointment);
//        patient.getAppointments().add(appointment);


        System.out.println(doctor.getName());
        System.out.println(patient.getPname());

        session.flush();
        session.clear();
        session.beginTransaction();
        session.saveOrUpdate(appointment);
        session.getTransaction().commit();

        //returns generated id value
        return appointment.getId();

    }

    private List<com.EntityClasses.Appointment> getAppointmentEntities(LocalDateTime startTime, LocalDateTime endTime){


        String hql = "FROM Appointment a where a.startTime between :startTime and :endTime";
        Query query = session.createQuery(hql);
        query.setParameter("startTime",Timestamp.valueOf(startTime));
        query.setParameter("endTime",Timestamp.valueOf(endTime));



        List results = query.list();
        return results;
    }



    private List<com.EntityClasses.Appointment> getDocAppointmentEntities(LocalDateTime startTime, LocalDateTime endTime,String Docname){


        String hql = "FROM Appointment a where a.startTime between :startTime and :endTime and a.doctor.name='"+Docname+"'";
        Query query = session.createQuery(hql);
        query.setParameter("startTime",Timestamp.valueOf(startTime));
        query.setParameter("endTime",Timestamp.valueOf(endTime));



        List results = query.list();
        return results;
    }

    public List<Appointment> getAppointments(LocalDateTime startTime,LocalDateTime endTime){
        List<com.EntityClasses.Appointment> entityList =getAppointmentEntities(startTime,endTime);
        List<Appointment> appointmentList = new ArrayList<>();



        for(com.EntityClasses.Appointment entity:entityList){

            Agenda.AppointmentImplLocal appointmentImplLocal= new Appointment()
                    .withStartLocalDateTime(entity.getStartTime().toLocalDateTime())
                    .withEndLocalDateTime(entity.getEndTime().toLocalDateTime())
                    .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group1"));
            appointmentImplLocal.setDescription(entity.getReason());

            Appointment appointment = (Appointment)appointmentImplLocal;
            appointment.setId(entity.getId());
            appointmentList.add(appointment);


        }

        return appointmentList;
    }



    public List<Appointment> getDocAppointments(LocalDateTime startTime,LocalDateTime endTime,String Docname){
        List<com.EntityClasses.Appointment> entityList1 =getDocAppointmentEntities(startTime,endTime,Docname);
        List<Appointment> appointmentList = new ArrayList<>();



        for(com.EntityClasses.Appointment entity:entityList1){

            Agenda.AppointmentImplLocal appointmentImplLocal= new Appointment()
                    .withStartLocalDateTime(entity.getStartTime().toLocalDateTime())
                    .withEndLocalDateTime(entity.getEndTime().toLocalDateTime())
                    .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group1"));
            appointmentImplLocal.setDescription(entity.getReason());

            Appointment appointment = (Appointment)appointmentImplLocal;
            appointment.setId(entity.getId());
            appointmentList.add(appointment);


        }

        return appointmentList;
    }

    public void deleteAppointment(int id){

        com.EntityClasses.Appointment entity = (com.EntityClasses.Appointment) session.get(com.EntityClasses.Appointment.class,id);

        session.beginTransaction();
        session.delete(entity);
        session.getTransaction().commit();
    }


    public void updateAppointment(Appointment newAppointment) {

        com.EntityClasses.Appointment entity = (com.EntityClasses.Appointment) session.get(com.EntityClasses.Appointment.class,newAppointment.getId());

        entity.setStartTime(Timestamp.valueOf(newAppointment.getStartLocalDateTime()));
        entity.setEndTime(Timestamp.valueOf(newAppointment.getEndLocalDateTime()));
        entity.setReason(newAppointment.getDescription());
        entity.setCharge(newAppointment.getCharge());
        entity.setDoctor(newAppointment.getDoctor());
        entity.setPatient(newAppointment.getPatient());

       if(entity==null)
           System.out.println("NULL");
       else{
           System.out.println(entity.getId() + entity.getStartTime().toLocalDateTime().toLocalDate().toString());
       }
           System.out.println(newAppointment.toString());

       try {
           session.beginTransaction();
           session.update(entity);
           session.getTransaction().commit();
       }catch (Exception e){
           e.printStackTrace();
       }


    }
}

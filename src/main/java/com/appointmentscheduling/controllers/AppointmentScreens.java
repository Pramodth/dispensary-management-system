package com.appointmentscheduling.controllers;

import com.common.BaseEnum;

/**
 * Based on 'Myscreens' Created by gayashan on 8/14/2017.
 */
public enum AppointmentScreens implements BaseEnum {

    AddAppointment("AddAppointment", "/com/appointmentscheduling/Add_Appointments/Add_Appointment.fxml"),
    AppointmentReports("AppointmentReports", "/com/appointmentscheduling/Add_Appointments/AppointmentReports.fxml"),

    ;


    String path;
    String id;

    AppointmentScreens(String id, String path) {
        this.path = path;
        this.id = id;
    }

    ;

    public String getPath() {
        return path;
    }

    public String getId() {
        return id;
    }
}

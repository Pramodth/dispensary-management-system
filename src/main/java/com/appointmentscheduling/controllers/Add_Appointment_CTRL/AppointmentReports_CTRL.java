package com.appointmentscheduling.controllers.Add_Appointment_CTRL;

import com.EntityClasses.Appointment;
import com.common.ScreenController;
import com.common.SessionListener;
//import com.itextpdf.*;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.main.controllers.MainScreenController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import jfxtras.scene.control.LocalDateTimeTextField;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.awt.event.ActionEvent;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.EntityClasses.Appointment;

public class AppointmentReports_CTRL implements Initializable, SessionListener {



    @FXML
    private JFXTextField PName;



    @FXML
    private TreeTableView<Appointment> Dtable;

    @FXML
    private Button DPrint;

    @FXML
    private LocalDateTimeTextField StartTime;

    @FXML
    private LocalDateTimeTextField EndTime;

    @FXML
    private JFXTextField DName;
    private Session session;
    private MainScreenController mainScreenController;
    @FXML
    private JFXTreeTableView<Appointment> Table;

    private TreeItem<Appointment> root;
    private final ObservableList<Appointment> patientList = FXCollections.observableArrayList();

    @Override
    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public void setMainController(SessionListener controller) {

        this.mainScreenController = (MainScreenController)controller;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.session = ScreenController.getSession();

///############################################################################################################################

        session.beginTransaction();

        Query query = session.createQuery("select a from Appointment a");
        List<Appointment> Appointments = query.list();

        session.getTransaction().commit();

        for (Appointment a : Appointments){
            patientList.add(a);
        }



        JFXTreeTableColumn<Appointment, Number> PTableAid =  new JFXTreeTableColumn<>("Name");
        PTableAid.setCellValueFactory(param -> param.getValue().getValue().idProperty());

        JFXTreeTableColumn<Appointment, String> PTableDname =  new JFXTreeTableColumn<>("NIC");
        PTableDname.setCellValueFactory(param -> param.getValue().getValue().getDoctor().nameProperty());

        JFXTreeTableColumn<Appointment, String> PTablePname =  new JFXTreeTableColumn<>("D.O.B");
        PTablePname.setCellValueFactory(param -> param.getValue().getValue().getPatient().pnameProperty());

        JFXTreeTableColumn<Appointment, String> PTableReason =  new JFXTreeTableColumn<>("Occupation");
        PTableReason.setCellValueFactory(param -> param.getValue().getValue().reasonProperty());

        JFXTreeTableColumn<Appointment, String> PTableGender =  new JFXTreeTableColumn<>("Contact");
        PTableGender.setCellValueFactory(param -> param.getValue().getValue().getDoctor().genderProperty());

        root = new RecursiveTreeItem<Appointment>(patientList, RecursiveTreeObject::getChildren);

        //noinspection unchecked
        Table.getColumns().setAll(PTableAid, PTableDname, PTablePname, PTableReason, PTableGender);
        Table.setRoot(root);
        Table.setShowRoot(false);


    }

    @FXML
    void SearchPatient (KeyEvent event) {

//        String name = PName.getText();
//
//        session.beginTransaction();
//
//        Query query = session.createQuery("from Appointment a where a.patient.pname like :keyword ");
//        query.setParameter("keyword","%" +name+ "%");
//
//        List<Appointment> MemberList = query.list();
//
//        session.getTransaction().commit();
//
//        Appointments.clear();
//        for (Appointment a : MemberList){
//            itemList.add(new TreeItem<>(a));
//        }
//
//
//        Table.getRoot().getChildren().clear();
//        Table.getRoot().getChildren().addAll(itemList);


    }

    @FXML
    void SearchDoc(KeyEvent event) {


//        String name = DName.getText();
//
//        session.beginTransaction();
//
//        Query query = session.createQuery("from Appointment a where a.patient.pname like :keyword  and a.startTime between :startTime and :endTime");
//        query.setParameter("keyword","%" +name+ "%");
//        query.setParameter("startTime", Timestamp.valueOf(StartTime.getLocalDateTime()));
//        query.setParameter("endTime",Timestamp.valueOf(EndTime.getLocalDateTime()));
//
//
//        List<Appointment> MemberList = query.list();
//
//        session.getTransaction().commit();
//
//        itemList.clear();;
//
//        Appointments.clear();
//        for (Appointment a : MemberList){
//            itemList.add(new TreeItem<>(a));
//        }
//
//
//        Dtable.getRoot().getChildren().clear();
//        Dtable.getRoot().getChildren().addAll(itemList);


    }




}

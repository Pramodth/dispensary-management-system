package com.appointmentscheduling.controllers.Add_Appointment_CTRL;

import com.EntityClasses.Doctor;
import com.EntityClasses.Patient;
import com.appointmentscheduling.controllers.AppointmentAgenda.Appointment;
import com.appointmentscheduling.controllers.AppointmentAgenda.AppointmentModel;
import com.common.AlertDialog;
import com.common.ScreenController;
import com.common.SessionListener;
import com.main.controllers.MainScreenController;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import jfxtras.scene.control.CalendarPicker;
import jfxtras.scene.control.LocalTimeTextField;
import jfxtras.scene.control.agenda.Agenda;
import org.controlsfx.control.textfield.TextFields;
import org.hibernate.Query;
import org.hibernate.Session;;

import javax.print.Doc;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Kalan on 9/25/2017.
 */
public class Add_Appointment_CTRL implements Initializable,SessionListener {


    AppointmentModel appointmentModel = new AppointmentModel();

    private List<Doctor> Doctor;

    private List<Patient> Patient;

    static Doctor summaryDoctor;

    static Patient summaryPatient;


    @FXML
    private TextField Hname;

    @FXML
    private Agenda agenda;

    private List<TreeItem<Doctor>> doctorList;

    private Session session;

    @FXML
    private CalendarPicker calendar;

    @FXML
    private JFXTextField DoctorSeach;

    @FXML
    private JFXTextField Dname;

    @FXML
    private JFXTextField Spec;

    @FXML
    private Label Chargers;

    @FXML
    private JFXButton Dnext;

    @FXML
    private TextArea Reason;

    @FXML
    private LocalTimeTextField StartTime;

    @FXML
    private LocalTimeTextField EndTime;

    @FXML
    private JFXButton Tback;

    @FXML
    private JFXButton TNext;

    @FXML
    private Label DOB;

    @FXML
    private JFXTextField PTextBox;

    @FXML
    private Label PContactNum;

    @FXML
    private JFXButton AddPatient;

    @FXML
    private JFXButton AddAppointment;

    @FXML
    private Label FulCharge;

    @FXML
    private JFXTextField FullCharge;

    @FXML
    private Label txtMsg;


    private Appointment selectedAppointment;
    private MainScreenController mainScreenController;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        session = ScreenController.getSession();

        try {
            updateAgenda();
        } catch (Exception e) {
            e.printStackTrace();

        }

        agenda.setAllowDragging(true);
        agenda.setAllowResize(true);
        agenda.newAppointmentCallbackProperty().set((localDateTimeRange) -> {
            Agenda.AppointmentImplLocal appointmentImplLocal = new Appointment()
                    .withStartLocalDateTime(localDateTimeRange.getStartLocalDateTime())
                    .withEndLocalDateTime(localDateTimeRange.getEndLocalDateTime())
                    .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group1"));


            int id = appointmentModel.addNewAppointment(appointmentImplLocal);

            Appointment a = (Appointment) appointmentImplLocal;
            a.setId(id);

            StartTime.setLocalTime(appointmentImplLocal.getStartLocalDateTime().toLocalTime());
            EndTime.setLocalTime(appointmentImplLocal.getEndLocalDateTime().toLocalTime());
            appointmentImplLocal.setDescription(Reason.getText());

            return a;

        });


        //agenda.setMinHeight(2500);

        calendar.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {

            Date cal = calendar.getCalendar().getTime();
            LocalDate ld = cal.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            LocalTime lt = LocalTime.NOON;

            agenda.setDisplayedLocalDateTime(LocalDateTime.of(ld, lt));

            updateAgenda();


        });

        agenda.appointmentChangedCallbackProperty().set(param -> {


                    appointmentModel.updateAppointment((Appointment) param);
                    return null;
                }
        );

        agenda.actionCallbackProperty().set(param ->
                {
                    selectedAppointment = (Appointment) param;
                    StartTime.setLocalTime(selectedAppointment.getStartLocalDateTime().toLocalTime());
                    EndTime.setLocalTime(selectedAppointment.getEndLocalDateTime().toLocalTime());
                    Reason.setText(selectedAppointment.getDescription());
                    Dname.setText(selectedAppointment.getDoctor().getName());
                    Chargers.setText(Double.toString(selectedAppointment.getDoctor().getChargePerVisit()));
                    PTextBox.setText(selectedAppointment.getPatient().getPname());
                    PContactNum.setText(selectedAppointment.getPatient().getContactNumber());
                    PContactNum.setText(selectedAppointment.getPatient().getContactNumber());
                    //DOB.setText(date.toString(selectedAppointment.getPatient().getDOB()));
                    FulCharge.setText(Double.toString(selectedAppointment.getCharge()));
                    return null;
                }
        );


//############ POPULATING ADD APPOINTMENT DOCTOR SEARCH BAR ####################################

        List<String> DNames;
        session.beginTransaction();
        Query DoctorNameQuery = session.createQuery("select d.name  from Doctor d");
        DNames = DoctorNameQuery.list();
        session.getTransaction().commit();

        TextFields.bindAutoCompletion(Dname, DNames);


//############ POPULATING ADD APPOINTMENT PATIENT SEARCH BAR ####################################

        List<String> PNames;
        session.beginTransaction();
        Query patientNameQuery = session.createQuery("select p.pname  from Patient p");
        PNames = patientNameQuery.list();
        session.getTransaction().commit();

        TextFields.bindAutoCompletion(PTextBox, PNames);


//######################## POPULATING DOCTOR SEARCH BAR #######################################

        List<String> DSNames;
        session.beginTransaction();
        Query DoctorSNameQuery = session.createQuery("select d.name  from Doctor d");
        DSNames = DoctorSNameQuery.list();
        session.getTransaction().commit();

        TextFields.bindAutoCompletion(DoctorSeach, DSNames);


    }


    @FXML
    void updateAppointment(ActionEvent event) {
        Date selected;
        if (calendar.getCalendar() == null) {
            selected = Date.from(selectedAppointment.getStartLocalDateTime().toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant());

        } else {
            selected = calendar.getCalendar().getTime();
        }
        LocalDate date = selected.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        selectedAppointment.setStartLocalDateTime(StartTime.getLocalTime().atDate(date));
        selectedAppointment.setEndLocalDateTime(EndTime.getLocalTime().atDate(date));
        selectedAppointment.setDescription(Reason.getText());
        //       selectedAppointment.setDoctor(Add_Appointment_CTRL.getDocClass(Dname.getText()));
//        selectedAppointment.setPatient(Add_Appointment_CTRL.getPatientClass(PTextBox.getText()));
        selectedAppointment.setCharge(Double.parseDouble(FullCharge.getText()));
        appointmentModel.updateAppointment(selectedAppointment);
        updateAgenda();
        agenda.refresh();


    }


    @FXML
    void deleteAppointment(ActionEvent event) {

        appointmentModel.deleteAppointment(selectedAppointment.getId());
        updateAgenda();
        agenda.refresh();

    }


    @FXML
    void addAppointment(ActionEvent event) {

        addAP();
        Date selected = calendar.getCalendar().getTime();
        LocalDate date = selected.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Agenda.AppointmentImplLocal newAppointment = new Appointment()
                .withStartLocalDateTime(StartTime.getLocalTime().atDate(date))
                .withEndLocalDateTime(EndTime.getLocalTime().atDate(date))
                .withDescription(Reason.getText())
                .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group1"));

        session.flush();
        session.clear();
        session.beginTransaction();
        Query patientNameQuery = session.createQuery(" from Patient p where p.pname = :name");
        patientNameQuery.setParameter("name", PTextBox.getText());

        List<Patient> patients = patientNameQuery.list();
        session.getTransaction().commit();
        AppointmentModel.patient = patients.get(0);

        session.flush();
        session.clear();
        session.beginTransaction();
        Query doctorNameQuery = session.createQuery(" from Doctor p ");
//        patientNameQuery.setParameter("name",Dname.getText());

        List<Doctor> doctors = doctorNameQuery.list();
        session.getTransaction().commit();
        AppointmentModel.doctor = doctors.get(0);


        agenda.appointments().add(newAppointment);
        appointmentModel.addNewAppointment(newAppointment);


    }


    private void updateAgenda() {
        agenda.localDateTimeRangeCallbackProperty().set(param -> {


                    List<Appointment> list = appointmentModel.getAppointments(param.getStartLocalDateTime(), param.getEndLocalDateTime());
                    agenda.appointments().clear();
                    agenda.appointments().addAll(list);
                    return null;
                }

        );
    }


//    @FXML
//    void SearchDoc(MouseEvent event) {
//
//        agenda.localDateTimeRangeCallbackProperty().set(param -> {
//
//
//                    List<Appointment> list = appointmentModel.getDocAppointmentEntities(param.getStartLocalDateTime(), param.getEndLocalDateTime(),param.);
//                    agenda.appointments().clear();
//                    agenda.appointments().addAll(list);
//                    return null;
//                }
//
//        );
//
//
//    }





    @Override
    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public void setMainController(SessionListener controller) {

        this.mainScreenController = (MainScreenController) controller;
    }


    public String getcharge() {
        return FullCharge.getText();
    }

    public Doctor getDocClass(String name) {


        session.beginTransaction();
        Query patientNameQuery = session.createQuery("select d from Doctor d where d.name = '" + name + "'");
        Doctor = patientNameQuery.list();
        session.getTransaction().commit();

        if (!(Doctor.isEmpty())) {

            summaryDoctor = Doctor.get(0);

            return summaryDoctor;
        } else {
            AlertDialog.show("Alert", "There is no such Doctor");
        }

        return null;

    }


    public Patient getPatientClass(String name) {


        session.beginTransaction();
        Query patientNameQuery = session.createQuery("select p from Patient p where p.pname = '" + name + "'");
        Doctor = patientNameQuery.list();
        session.getTransaction().commit();

        if (!(Patient.isEmpty())) {

            summaryPatient = Patient.get(0);

            return summaryPatient;
        } else {
            AlertDialog.show("Alert", "There is no such Patient");
        }

        return null;

    }


    private boolean isValidDName() {

        boolean flag = false;

        String regex = "^[a-zA-Z]+$";

        if (Dname.getText().matches(regex)) {

            flag = true;
        } else {

            txtMsg.setText("*Please Enter a Valid Doctor Name");
        }
        return flag;
    }


    private boolean isValidPName() {

        boolean flag = false;

        String regex = "^[a-zA-Z]+$";

        if (PTextBox.getText().matches(regex)) {

            flag = true;
        } else {

            txtMsg.setText("*Please Enter a Valid Patient Name");
        }
        return flag;
    }


    @FXML
    void addAP() {

        if (isValidDName()) {
            if (isValidPName()) {

            }
        }
    }


    @FXML
    void populateDoctor() {

        String patientName = Dname.getText();

        if (!(Dname.getText().equals(""))) {

            List<Doctor> Doctor;

            session.clear();
            session.beginTransaction();
            Query patientNameQuery = session.createQuery("select p from Doctor p where p.name = '" + patientName + "'");
            Doctor = patientNameQuery.list();
            session.getTransaction().commit();

            if (!(Doctor.isEmpty())) {

                summaryDoctor = Doctor.get(0);

                Chargers.setText(Double.toString(summaryDoctor.getChargePerVisit()));

            } else {

            }
        } else {

        }


    }



    @FXML
    void populatePatient() {

        String patientName = PTextBox.getText();

        if (!(PTextBox.getText().equals(""))) {

            List<Patient> patients;

            session.clear();
            session.beginTransaction();
            Query patientNameQuery = session.createQuery("select p from Patient p where p.pname = '" + patientName + "'");
            patients = patientNameQuery.list();
            session.getTransaction().commit();

            if (!(patients.isEmpty())) {

                summaryPatient = patients.get(0);

                PContactNum.setText(summaryPatient.getContactNumber());
                DOB.setText(summaryPatient.getDOB().toString());

            } else {

            }
        } else {

        }


    }

}
package com.EntityClasses;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by Damma on 8/31/2017.
 */

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "Appointment")
public class Appointment extends RecursiveTreeObject<Appointment> {


    private IntegerProperty id;
    private SimpleDoubleProperty charge;
    private SimpleStringProperty reason;
    private Timestamp startTime;
    private Timestamp endTime;

    private Doctor doctor;
    private Patient patient;

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Appointment() {

        this.charge = new SimpleDoubleProperty();
        this.id = new SimpleIntegerProperty();
        this.reason = new SimpleStringProperty();


    }

    @Column(name = "reason")
    public String getReason() {
        return reason.get();
    }

    public SimpleStringProperty reasonProperty() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason.set(reason);
    }

    @Column(name = "charge")
    public double getCharge() {
        return charge.get();
    }

    public SimpleDoubleProperty chargeProperty() {
        return charge;
    }

    public void setCharge(double charge) {
        this.charge.set(charge);
    }

    @Id
    @GeneratedValue
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
